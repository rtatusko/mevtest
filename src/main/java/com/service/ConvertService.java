package com.service;

import com.entity.User;
import com.github.vincentrussell.query.mongodb.sql.converter.MongoDBQueryHolder;
import com.github.vincentrussell.query.mongodb.sql.converter.ParseException;
import com.github.vincentrussell.query.mongodb.sql.converter.QueryConverter;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.mongodb.*;
import com.repo.IUserRepo;
import org.apache.commons.cli.CommandLine;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class ConvertService {

    @Autowired
    private IUserRepo iUserRepo;

//    private final String sqlQuery = "select name from user ";
//
//    //db.user.find({age :{$gt: 13}})
//    private final String sqlQuery1 = "select name from user where age > 10 ";
//
//    private final String sqlQuery2 = "select name, surname from user where age >= 10 order by age asc";
//
//    //  findAll db.user.findAll()
//    private final String sqlQuery3 = "select * from user";



    public void getCon() {
        // or
        MongoClient mongoClient;
        mongoClient = new MongoClient("localhost", 27017);
        DB db = mongoClient.getDB("testdb");
        DBCollection coll = db.getCollection("user");

//            BasicDBObject doc = new BasicDBObject("name", "MongoDB")
//                    .append("type", "database")
//                    .append("count", 1)
//                    .append("info",
//                            new BasicDBObject("x", 203).append("y", 102));
//            coll.insert(doc);

        List<DBObject> all = coll.find().toArray();
        String s = (String) all.get(0).get("name");
    }

    public void parceQuery () throws ParseException {


        List<User> users  = iUserRepo.findAll();

//
//        MongoClient mongoClient = null;
//        QueryConverter queryConverter = new QueryConverter("select age from user");
//
//        CommandLine cmd = null;
//
//        mongoClient = getMongoClient();
//        Object result = queryConverter.run(mongoClient.getDatabase("testdb"));
//
//        MongoDBQueryHolder mongoDBQueryHolder = queryConverter.getMongoQuery();
//
//        String collection = mongoDBQueryHolder.getCollection();
//        Document query = mongoDBQueryHolder.getQuery();
//        Document projection = mongoDBQueryHolder.getProjection();
//        Document sort = mongoDBQueryHolder.getSort();
    }

    private static MongoClient getMongoClient() {
        final Pattern hostAndPort = Pattern.compile("^(.[^:]*){1}([:]){0,1}(\\d+){0,1}$");
        List<ServerAddress> serverAddresses = Lists.transform(Arrays.asList("localhost:27017"), new Function<String, ServerAddress>() {
            @Override
            public ServerAddress apply(String string) {
                Matcher matcher = hostAndPort.matcher(string.trim());
                if (matcher.matches()) {
                    String hostname = matcher.group(1);
                    String port = matcher.group(3);
                    return new ServerAddress(hostname,port!=null ? Integer.parseInt(port) : Integer.parseInt("27017"));

                } else {
                    throw new IllegalArgumentException(string + " doesn't appear to be a hostname.");
                }
            }
        });

            return new MongoClient(serverAddresses);

    }
}
