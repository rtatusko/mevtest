package com.repo;

import com.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IUserRepo extends MongoRepository<User, String> {
}
